<?php

namespace App;

class BackstagePass extends Item
{
   public function __construct($quality, $sellIn)
    {
        $this->quality = $quality;
        $this->sellIn = $sellIn;
    }
    public function tick(){
        $this->sellIn -=1;
        if( $this->sellIn < 0){
            $this->quality = 0;
            return;
        }
        if( $this->quality >= 50)
            return;
        if($this->sellIn < 5){
             $this->quality += 3;
        }
        else if($this->sellIn < 10){
            $this->quality += 2;
        }
        else{
            $this->quality += 1;
        }
   
   
    }
}